<?php

declare(strict_types=1);

namespace OpenSearch;

class Suggestion
{
    private string $term;
    private string $description;
    private string $url;

    public function __construct(string $term, string $description = "", string $url = "")
    {
        $this->term = $term;
        $this->description = $description;
        $this->url = $url;
    }

    public function getTerm(): string
    {
        return $this->term;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }
}