<?php

declare(strict_types=1);

namespace OpenSearch;

enum UrlResource: string
{
    case RESULTS = "results";
    case SUGGESTIONS = "suggestions";
    case SELF = "self";
    case COLLECTION = "collection";
}