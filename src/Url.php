<?php

declare(strict_types=1);

namespace OpenSearch;

class Url
{
    private string $template;
    private string $type;
    private UrlResource $rel;
    private int $indexOffset;
    private int $pageOffset;

    public static function fromXml(\SimpleXMLElement $element): self
    {
        $a = [];
        foreach ($element->attributes() as $key => $val) {
            $a[$key] = (string)$val;
        }
        $template = $a["template"] ?? null;
        $type = $a["type"] ?? null;
        $rel = $a["rel"] ?? null;
        $indexOffset = $a["type"] ?? null;
        $pageOffset = $a["pageOffset"] ?? null;
        return new self(
            $template,
            $type,
            $rel ? UrlResource::from($rel) : UrlResource::RESULTS,
            (null === $indexOffset) ? 1 : (int)$indexOffset,
            (null === $pageOffset) ? 1 : (int)$pageOffset,
        );
    }

    /**
     * @param string $template
     * @param string $type
     * @param UrlResource $rel
     * @param int $indexOffset
     * @param int $pageOffset
     */
    public function __construct(
        string $template,
        string $type,
        UrlResource $rel = UrlResource::RESULTS,
        int $indexOffset = 1,
        int $pageOffset = 1,
    ) {
        $this->template = $template;
        $this->type = $type;
        $this->rel = $rel;
        $this->indexOffset = $indexOffset;
        $this->pageOffset = $pageOffset;
    }

    public function getTemplate(): string
    {
        return $this->template;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getRel(): UrlResource
    {
        return $this->rel;
    }

    public function getIndexOffset(): ?int
    {
        return $this->indexOffset;
    }

    public function getPageOffset(): ?int
    {
        return $this->pageOffset;
    }
}