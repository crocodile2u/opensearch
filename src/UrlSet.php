<?php

declare(strict_types=1);

namespace OpenSearch;

class UrlSet implements \Countable, \IteratorAggregate
{
    /**
     * @var Url[]
     */
    private $urls;

    public static function fromXml(\SimpleXMLElement $element): self
    {
        $urls = [];
        foreach ($element as $url) {
            $urls[] = Url::fromXml($url);
        }
        return new self(...$urls);
    }

    public function __construct(Url ...$urls)
    {
        $this->urls = $urls;
    }

    public function count()
    {
        return count($this->urls);
    }

    /**
     * @return \Generator<Url>
     */
    public function getIterator(): \Generator
    {
        yield from $this->urls;
    }

    public function at(int $index): Url
    {
        return $this->urls[$index];
    }
}