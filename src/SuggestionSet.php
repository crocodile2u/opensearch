<?php

declare(strict_types=1);

namespace OpenSearch;

class SuggestionSet implements \JsonSerializable
{
    private string $prefix;
    private array $suggestions = [];

    public function __construct(string $prefix)
    {
        $this->prefix = $prefix;
    }

    public function add(string $term, string $description = "", string $url = ""): self
    {
        $this->suggestions[] = new Suggestion($term, $description, $url);
        return $this;
    }

    public function jsonSerialize()
    {
        $ret = [
            $this->prefix,
            $this->terms(),
        ];
        $descriptions = $this->descriptions();
        $hasDescriptions = count(array_filter($descriptions)) > 0;
        $urls = $this->urls();
        $hasUrls = count(array_filter($urls)) > 0;
        if ($hasDescriptions || $hasUrls) {
            $ret[] = $descriptions;
        }
        if ($hasUrls) {
            $ret[] = $urls;
        }
        return $ret;
    }

    public function terms()
    {
        return array_map(fn(Suggestion $s) => $s->getTerm(), $this->suggestions);
    }

    public function descriptions()
    {
        return array_map(fn(Suggestion $s) => $s->getDescription(), $this->suggestions);
    }

    public function urls()
    {
        return array_map(fn(Suggestion $s) => $s->getUrl(), $this->suggestions);
    }
}