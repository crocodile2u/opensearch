<?php

declare(strict_types=1);

namespace OpenSearch;

class SpecPlaceholderSet implements \Countable, \IteratorAggregate
{
    /**
     * @var SpecPlaceholder[]
     */
    private $specs;

    public function __construct(SpecPlaceholder ...$specs)
    {
        $this->specs = $specs;
    }

    public function resolve(): SpecSet
    {
        $specs = [];
        foreach ($this->specs as $spec) {
            $specs[] = Spec::fromUrl($spec->getUrl());
        }
        return new SpecSet(...$specs);
    }

    public function count()
    {
        return count($this->specs);
    }

    public function at(int $index): SpecPlaceholder
    {
        return $this->specs[$index];
    }

    /**
     * @return \Generator<SpecPlaceholder>
     */
    public function getIterator(): \Generator
    {
        yield from $this->specs;
    }
}