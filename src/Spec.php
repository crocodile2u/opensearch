<?php

declare(strict_types=1);

namespace OpenSearch;

class Spec
{
    /**
     * @var string source URL / file etc
     */
    private string $source = "";
    private string $shortName;
    private string $description;
    private UrlSet $urls;
    private string $tags;
    private string $longName;
    private ImageSet $images;
    private QuerySet $queries;
    private ?string $developer;
    private ?string $attribution;
    private string $sindicationRight;
    private bool $adultContent;
    private string $language;
    private string $inputEncoding;
    private string $outputEncoding;

    public static function fromUrl(string $url): self
    {
        return self::fromXML(simplexml_load_file($url))->setSource($url);
    }

    public static function fromFile(string $filename): self
    {
        if (!is_file($filename)) {
            throw new \InvalidArgumentException("$filename is not a file");
        }
        if (!is_readable($filename)) {
            throw new \InvalidArgumentException("$filename is not readable");
        }
        return self::fromXML(simplexml_load_file($filename))->setSource($filename);
    }

    public static function fromString(string $xml): self
    {
        return self::fromXML(simplexml_load_string($xml))->setSource("xml string");
    }

    public static function fromXML(\SimpleXMLElement $element): self
    {
        return new self(
            (string)$element->ShortName,
            (string)$element->Description,
            UrlSet::fromXml($element->Url),
            (string)$element->Tags,
            (string)$element->LongName,
            ImageSet::fromXml($element->Image),
            QuerySet::fromXml($element->Query),
            (string)$element->Developer,
            (string)$element->Attribution,
            (string)$element->SindicationRight ?: "open",
            filter_var((string)$element->AdultContent ?: "false", FILTER_VALIDATE_BOOL),
            (string)$element->Language ?: "*",
            (string)$element->InputEncoding ?: "UTF-8",
            (string)$element->OutputEncoding ?: "UTF-8",
        );
    }

    /**
     * @param string $shortName
     * @param string $description
     * @param UrlSet $urls
     * @param string $tags
     * @param string $longName
     * @param ImageSet $images
     * @param QuerySet $queries
     * @param string|null $developer
     * @param string|null $attribution
     * @param string $sindicationRight
     * @param bool $adultContent
     * @param string $language
     * @param string $inputEncoding
     * @param string $outputEncoding
     */
    public function __construct(
        string $shortName,
        string $description,
        UrlSet $urls,
        string $tags,
        string $longName,
        ImageSet $images,
        QuerySet $queries,
        ?string $developer,
        ?string $attribution,
        string $sindicationRight,
        bool $adultContent,
        string $language,
        string $inputEncoding,
        string $outputEncoding
    ) {
        $this->shortName = $shortName;
        $this->description = $description;
        $this->urls = $urls;
        $this->tags = $tags;
        $this->longName = $longName;
        $this->images = $images;
        $this->queries = $queries;
        $this->developer = $developer;
        $this->attribution = $attribution;
        $this->sindicationRight = $sindicationRight;
        $this->adultContent = $adultContent;
        $this->language = $language;
        $this->inputEncoding = $inputEncoding;
        $this->outputEncoding = $outputEncoding;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;
        return $this;
    }

    public function getShortName(): string
    {
        return $this->shortName;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getUrls(): UrlSet
    {
        return $this->urls;
    }

    public function getTags(): string
    {
        return $this->tags;
    }

    public function getLongName(): string
    {
        return $this->longName;
    }

    public function getImages(): ImageSet
    {
        return $this->images;
    }

    public function getQueries(): QuerySet
    {
        return $this->queries;
    }

    public function getDeveloper(): ?string
    {
        return $this->developer;
    }

    public function getAttribution(): ?string
    {
        return $this->attribution;
    }

    public function getSindicationRight(): string
    {
        return $this->sindicationRight;
    }

    public function isAdultContent(): bool
    {
        return $this->adultContent;
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function getInputEncoding(): string
    {
        return $this->inputEncoding;
    }

    public function getOutputEncoding(): string
    {
        return $this->outputEncoding;
    }
}