<?php

declare(strict_types=1);

namespace OpenSearch;

class SpecSet implements \Countable, \IteratorAggregate
{
    /**
     * @var Spec[]
     */
    private $specs;

    public function __construct(Spec ...$specs)
    {
        $this->specs = $specs;
    }

    public function count()
    {
        return count($this->specs);
    }

    /**
     * @return \Generator<Spec>
     */
    public function getIterator(): \Generator
    {
        yield from $this->specs;
    }

    public function at(int $index): Spec
    {
        return $this->specs[$index];
    }
}