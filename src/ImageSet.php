<?php

declare(strict_types=1);

namespace OpenSearch;

class ImageSet implements \Countable, \IteratorAggregate
{
    /**
     * @var Image[]
     */
    private $images;

    public static function fromXml(\SimpleXMLElement $element): self
    {
        $urls = [];
        foreach ($element as $url) {
            $urls[] = Image::fromXml($url);
        }
        return new self(...$urls);
    }

    public function __construct(Image ...$images)
    {
        $this->images = $images;
    }

    public function count()
    {
        return count($this->images);
    }

    /**
     * @return \Generator<Image>
     */
    public function getIterator(): \Generator
    {
        yield from $this->images;
    }
}