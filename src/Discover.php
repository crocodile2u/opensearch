<?php

declare(strict_types=1);

namespace OpenSearch;

use League\Uri\Uri;

class Discover
{
    public static function fromUrl(string $url): SpecPlaceholderSet
    {
        return self::fromHtml(file_get_contents($url), $url);
    }
    public static function fromHtml(string $html, string $sourceUrl): SpecPlaceholderSet
    {
        $dom = new \DOMDocument();
        $dom->loadHTML($html);
        $xml = simplexml_import_dom($dom);
        $specs = [];
        $source = Uri::createFromString($sourceUrl);
        foreach ($xml->head->link as $link) {
            if ("application/opensearchdescription+xml" !== (string)$link["type"]) {
                continue;
            }
            $href = Uri::createFromString((string)$link["href"]);
            $uri = Uri::createFromBaseUri($href, $source);
            $specs[] = new SpecPlaceholder($uri->__toString(), (string)$link["title"]);
        }
        return new SpecPlaceholderSet(...$specs);
    }
}