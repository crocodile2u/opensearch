<?php

declare(strict_types=1);

namespace OpenSearch;

class Query
{
    private string $role;
    private ?string $title;
    private ?int $totalResults;
    private ?string $searchTerms;
    private ?int $count;
    private ?int $startIndex;
    private ?int $startPage;
    private ?string $language;
    private ?string $inputEncoding;
    private ?string $outputEncoding;

    public static function fromXml(\SimpleXMLElement $element): self
    {
        $a = [];
        foreach ($element->attributes() as $key => $val) {
            $a[$key] = (string)$val;
        }
        $role = $a["role"] ?? null;
        $title = $a["title"] ?? null;
        $totalResults = $a["totalResults"] ?? null;
        $searchTerms = $a["searchTerms"] ?? null;
        $count = $a["count"] ?? null;
        $startIndex = $a["startIndex"] ?? null;
        $startPage = $a["startPage"] ?? null;
        $language = $a["language"] ?? null;
        $inputEncoding = $a["inputEncoding"] ?? null;
        $outputEncoding = $a["outputEncoding"] ?? null;
        return new self(
            $role,
            (null === $title) ? null : (string)$title,
            (null === $totalResults) ? null : (int)$totalResults,
            (null === $searchTerms) ? null : (string)$searchTerms,
            (null === $count) ? null : (int)$count,
            (null === $startIndex) ? null : (int)$startIndex,
            (null === $startPage) ? null : (int)$startPage,
            (null === $language) ? null : (string)$language,
            (null === $inputEncoding) ? null : (string)$inputEncoding,
            (null === $outputEncoding) ? null : (string)$outputEncoding,
        );
    }

    /**
     * @param string $role
     * @param string $title
     * @param int|null $totalResults
     * @param string|null $searchTerms
     * @param int|null $count
     * @param int|null $startIndex
     * @param int|null $startPage
     * @param string|null $language
     * @param string|null $inputEncoding
     * @param string|null $outputEncoding
     */
    public function __construct(
        string $role,
        ?string $title = null,
        ?int $totalResults = null,
        ?string $searchTerms = null,
        ?int $count = null,
        ?int $startIndex = null,
        ?int $startPage = null,
        ?string $language = null,
        ?string $inputEncoding = null,
        ?string $outputEncoding = null
    ) {
        $this->role = $role;
        $this->title = $title;
        $this->totalResults = $totalResults;
        $this->searchTerms = $searchTerms;
        $this->count = $count;
        $this->startIndex = $startIndex;
        $this->startPage = $startPage;
        $this->language = $language;
        $this->inputEncoding = $inputEncoding;
        $this->outputEncoding = $outputEncoding;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return int|null
     */
    public function getTotalResults(): ?int
    {
        return $this->totalResults;
    }

    /**
     * @return string|null
     */
    public function getSearchTerms(): ?string
    {
        return $this->searchTerms;
    }

    /**
     * @return int|null
     */
    public function getCount(): ?int
    {
        return $this->count;
    }

    /**
     * @return int|null
     */
    public function getStartIndex(): ?int
    {
        return $this->startIndex;
    }

    /**
     * @return int|null
     */
    public function getStartPage(): ?int
    {
        return $this->startPage;
    }

    /**
     * @return string|null
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    /**
     * @return string|null
     */
    public function getInputEncoding(): ?string
    {
        return $this->inputEncoding;
    }

    /**
     * @return string|null
     */
    public function getOutputEncoding(): ?string
    {
        return $this->outputEncoding;
    }
}