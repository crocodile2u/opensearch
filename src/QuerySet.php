<?php

declare(strict_types=1);

namespace OpenSearch;

class QuerySet implements \Countable, \IteratorAggregate
{
    /**
     * @var Query[]
     */
    private $queries;

    public static function fromXml(\SimpleXMLElement $element): self
    {
        $urls = [];
        foreach ($element as $url) {
            $urls[] = Query::fromXml($url);
        }
        return new self(...$urls);
    }

    public function __construct(Query ...$queries)
    {
        $this->queries = $queries;
    }

    public function count()
    {
        return count($this->queries);
    }

    /**
     * @return \Generator<Query>
     */
    public function getIterator(): \Generator
    {
        yield from $this->queries;
    }
}