<?php

declare(strict_types=1);

namespace OpenSearch;

class Image
{
    private string $url;
    private ?int $width;
    private ?int $height;
    private ?string $type;

    public static function fromXml(\SimpleXMLElement $element): self
    {
        $a = [];
        foreach ($element->attributes() as $key => $val) {
            $a[$key] = (string)$val;
        }
        $url = (string)$element;
        $width = $a["width"] ?? null;
        $height = $a["height"] ?? null;
        $type = $a["type"] ?? null;
        return new self(
            $url,
            (null === $width) ? null : (int)$width,
            (null === $height) ? null : (int)$height,
            (null === $type) ? null : (string)$type,
        );
    }

    /**
     * @param string $url
     * @param int|null $width
     * @param int|null $height
     * @param string|null $type
     */
    public function __construct(string $url, ?int $width = null, ?int $height = null, ?string $type = null)
    {
        $this->url = $url;
        $this->width = $width;
        $this->height = $height;
        $this->type = $type;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function getType(): ?string
    {
        return $this->type;
    }
}