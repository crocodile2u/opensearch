# opensearch

PHP [OpenSearch](https://developer.mozilla.org/en-US/docs/Web/OpenSearch) tools:

- autodiscovery
- spec parser
- suggestions JSON formatter (https://github.com/dewitt/opensearch/blob/master/mediawiki/Specifications/OpenSearch/Extensions/Suggestions)