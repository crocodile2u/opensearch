<?php

namespace OpenSearch;

use PHPUnit\Framework\TestCase;

class DiscoverTest extends TestCase
{
    public function testFromUrl()
    {
        $url = 'file://' . __DIR__ . '/samples/index.html';
        $expected = new SpecPlaceholderSet(
            new SpecPlaceholder("file:///home/victor/projects/opensearch/tests/OpenSearch/samples/mysiteauthor.xml", "MySite: By Author")
        );
        $actual = Discover::fromUrl($url);
        $this->assertEquals(count($actual), count($expected));
        /** @var SpecPlaceholder $sp */
        foreach ($actual as $i => $sp) {
            $this->assertEquals($expected->at($i)->getUrl(), $sp->getUrl());
            $this->assertEquals($expected->at($i)->getTitle(), $sp->getTitle());
        }
    }
    /**
     * @param string $html
     * @param string $sourceUrl
     * @param SpecPlaceholderSet $expected
     * @dataProvider provider_FromHtml
     */
    public function testFromHtml(string $html, string $sourceUrl, SpecPlaceholderSet $expected)
    {
        $sps = Discover::fromHtml($html, $sourceUrl);
        $this->assertEquals(count($sps), count($expected));
        /** @var SpecPlaceholder $sp */
        foreach ($sps as $i => $sp) {
            $this->assertEquals($expected->at($i)->getUrl(), $sp->getUrl());
            $this->assertEquals($expected->at($i)->getTitle(), $sp->getTitle());
        }
    }

    public function provider_FromHtml()
    {
        return [
            [
                '<html><head><link rel="search" type="application/opensearchdescription+xml"
      title="MySite: By Author" href="http://example.com/mysiteauthor.xml"></head><body></body></html>',
                'http://example.com/',
                new SpecPlaceholderSet(
                    new SpecPlaceholder("http://example.com/mysiteauthor.xml", "MySite: By Author")
                ),
            ],
            [
                '<html><head><link rel="search" type="application/opensearchdescription+xml"
      title="MySite: By Author" href="/mysiteauthor.xml"></head><body></body></html>',
                'http://example.com/',
                new SpecPlaceholderSet(
                    new SpecPlaceholder("http://example.com/mysiteauthor.xml", "MySite: By Author")
                ),
            ],
            [
                '<html><head>
        <link rel="search" type="application/opensearchdescription+xml"
      title="MySite: By Author" href="/mysiteauthor.xml">
      <link rel="search" type="application/opensearchdescription+xml"
      title="MySite: By Comment" href="/mysitecomment.xml"></head><body></body></html>',
                'http://example.com/',
                new SpecPlaceholderSet(
                    new SpecPlaceholder("http://example.com/mysiteauthor.xml", "MySite: By Author"),
                    new SpecPlaceholder("http://example.com/mysitecomment.xml", "MySite: By Comment"),
                ),
            ],
        ];
    }
}
