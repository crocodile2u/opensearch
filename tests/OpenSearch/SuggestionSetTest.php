<?php

namespace OpenSearch;

use PHPUnit\Framework\TestCase;

class SuggestionSetTest extends TestCase
{
    /**
     * @param SuggestionSet $set
     * @param array $expected
     * @dataProvider provide_JsonSerialize
     */
    public function testJsonSerialize(SuggestionSet $set, array $expected)
    {
        $this->assertEquals($expected, $set->jsonSerialize());
    }

    public function provide_JsonSerialize()
    {
        return [
            [
                (new SuggestionSet("app"))
                    ->add("apple", "10 results", "http://example.com/search?apple")
                    ->add("application", "15 results", "http://example.com/search?application"),
                [
                    "app",
                    ["apple", "application"],
                    ["10 results", "15 results"],
                    ["http://example.com/search?apple", "http://example.com/search?application"],
                ],
            ],
            [
                (new SuggestionSet("app"))
                    ->add("apple", "10 results")
                    ->add("application", "15 results"),
                [
                    "app",
                    ["apple", "application"],
                    ["10 results", "15 results"],
                ],
            ],
            [
                (new SuggestionSet("app"))
                    ->add("apple", "", "http://example.com/search?apple")
                    ->add("application", "", "http://example.com/search?application"),
                [
                    "app",
                    ["apple", "application"],
                    ["", ""],
                    ["http://example.com/search?apple", "http://example.com/search?application"],
                ],
            ],
            [
                (new SuggestionSet("app"))
                    ->add("apple")
                    ->add("application"),
                [
                    "app",
                    ["apple", "application"],
                ],
            ],
        ];
    }
}
