<?php

namespace OpenSearch;

use PHPUnit\Framework\TestCase;

class SpecPlaceholderSetTest extends TestCase
{

    public function testResolve()
    {
        $url = 'file://' . __DIR__ . '/samples/index.html';
        $ss = Discover::fromUrl($url)->resolve();
        $this->assertCount(1, $ss);
        $first = $ss->at(0);
        $this->assertEquals("http://example.com/?q={searchTerms}", $first->getUrls()->at(0)->getTemplate());
        $this->assertEquals("My Site Search", $first->getShortName());
        $this->assertEquals("My Site Search Description", $first->getDescription());
    }
}
